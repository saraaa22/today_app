import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: HomePage(),
  ));
}
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    void click(){}
    var width = MediaQuery
        .of(context)
        .size
        .width;

    item(){
      return Container(
        child:Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                width: width*2/3,
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(10.0),
                      child: ListTile(
                        title: Text('Today', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 36.0)),
                        subtitle: Text('Productive Day,Philip', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0,color: Color(0x8c8d8f).withOpacity(1.0))) ,
                      ),
                    ),

                  ],
                ),
              ),
              Container(
                child: RaisedButton(
                  onPressed: click,
                  child: Text('Add task',
                    style: TextStyle(color: Colors.white),),
                  color: Color(0X309397).withOpacity(1.0),
                  shape: new RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(30.0),
                ),),
              )
            ]
        ),
      );
    }



    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        elevation: 0,
        title: IconButton(icon: Icon(Icons.arrow_back_ios),color: Colors.black, onPressed: click),
        backgroundColor: Color(0xffffff).withOpacity(1.0),
        iconTheme: IconThemeData(color: Color(0X925F11).withOpacity(1.0)),
      ),
      body: ListView(
      padding: EdgeInsets.all(10.0),
        children: <Widget>[
          Container(
            child: item(),
          )
        ],
      ),
    );
  }
}

